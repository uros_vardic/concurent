package threads;

import java.util.concurrent.CyclicBarrier;

import static util.Preconditions.checkNotNull;

public class Professor implements Runnable {

    private static final int PARTIES = 2;

    private final Classroom classroom;

    private final CyclicBarrier inBarrier = new CyclicBarrier(PARTIES);

    private final CyclicBarrier outBarrier = new CyclicBarrier(PARTIES);

    Professor(Classroom classroom) {
        this.classroom = checkNotNull(classroom);
    }

    CyclicBarrier getInBarrier() {
        return inBarrier;
    }

    CyclicBarrier getOutBarrier() {
        return outBarrier;
    }

    private boolean professorReady = true;

    boolean isProfessorReady() {
        return professorReady;
    }

    void setProfessorReady(boolean professorReady) {
        this.professorReady = professorReady;
    }

    @Override
    public void run() {
        synchronized (classroom.getLock()) {
            classroom.getLock().notify();
        }
    }
}
