package threads;

import static util.Preconditions.checkNotNull;

class Assistant implements Runnable {

    private final Classroom classroom;

    Assistant(Classroom classroom) {
        this.classroom = checkNotNull(classroom);
    }

    private boolean assistantReady = true;

    boolean isAssistantReady() {
        return assistantReady;
    }

    void setAssistantReady(boolean assistantReady) {
        this.assistantReady = assistantReady;
    }

    @Override
    public void run() {
        synchronized (classroom.getLock()) {
            classroom.getLock().notify();
        }
    }
}
