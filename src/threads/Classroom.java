package threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class Classroom implements Callable<String> {

    private final int numberOfStudents;

    private final Professor professor;

    private final Assistant assistant;

    private final ScheduledExecutorService studentExecutor;

    private final ExecutorService teacherExecutor;

    private final Object lock = new Object();

    private final List<Student> students = new ArrayList<>();

    private final List<Student> arrivedStudents = new ArrayList<>();

    public Classroom(int numberOfStudents) {
        checkArgument(numberOfStudents > 0, "Number of students must be a positive number!");

        this.numberOfStudents = numberOfStudents;
        this.professor        = new Professor(this);
        this.assistant        = new Assistant(this);
        this.studentExecutor  = Executors.newScheduledThreadPool(numberOfStudents);
        this.teacherExecutor  = Executors.newFixedThreadPool(2);

        initializeStudents();
    }

    private void initializeStudents() {
        IntStream.range(0, numberOfStudents)
                .mapToObj(counter -> new Student(this))
                .forEach(students::add);
    }

    Professor getProfessor() {
        return professor;
    }

    Assistant getAssistant() {
        return assistant;
    }

    Object getLock() {
        return lock;
    }

    List<Student> getStudents() {
        return Collections.unmodifiableList(students);
    }

    synchronized void studentArrivedOnTime(Student student) {
        checkNotNull(student);
        checkArgument(!arrivedStudents.contains(student), "Student already started!");

        arrivedStudents.add(student);
    }

    void callForProfessor() {
        teacherExecutor.submit(professor);
    }

    void callForAssistant() {
        teacherExecutor.submit(assistant);
    }

    private int scoreSum;

    synchronized void addScore(int score) {
        checkArgument(score >= 0 && score <= 10, "Score interval must be (0, 10)!");

        scoreSum += score;
    }

    @Override
    public String call() throws Exception {
        students.forEach(this::callForStudent);

        studentExecutor.shutdown();

        synchronized (this) {
            wait();
        }

        teacherExecutor.shutdown();

        return "Average score: " + scoreSum / numberOfStudents;
    }

    private void callForStudent(Student student) {
        studentExecutor.schedule(student, student.getDelayTime(), TimeUnit.MILLISECONDS);
    }

    public void shutdownClassroom() {
        studentExecutor.shutdownNow();
        teacherExecutor.shutdownNow();
    }

    public void printLateStudents() {
        final List<Student> lateStudents = new ArrayList<>(students);

        lateStudents.removeAll(arrivedStudents);

        lateStudents.forEach(student -> System.err.println("Late: " + student));
    }

}
