package threads;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static util.Preconditions.checkNotNull;

class Student implements Runnable {

    private static final long MIN_PRESENTATION_TIME = 500L;

    private static final long MAX_PRESENTATION_TIME = 1000L;

    private static final long MIN_DELAY_TIME = 0L;

    private static final long MAX_DELAY_TIME = 1000L;

    private static final long PROFESSOR_TIMEOUT = 500L;

    private final Classroom classroom;

    private final long presentationTime;

    private final long delayTime;

    Student(Classroom classroom) {
        this.classroom        = checkNotNull(classroom);
        this.presentationTime = calculatePresentationTime();
        this.delayTime        = calculateDelayTime();
    }

    private long calculatePresentationTime() {
        return ThreadLocalRandom.current().nextLong(MIN_PRESENTATION_TIME, MAX_PRESENTATION_TIME);
    }

    private long calculateDelayTime() {
        return ThreadLocalRandom.current().nextLong(MIN_DELAY_TIME, MAX_DELAY_TIME);
    }

    long getDelayTime() {
        return delayTime;
    }

    @Override
    public void run() {
        classroom.studentArrivedOnTime(this);

        try {
            waitToPresent();

            if (classroom.getProfessor().isProfessorReady())
                presentToProfessor();
            else
                presentToAssistant();

        } catch (InterruptedException e) {
            timeoutPresentation(10);
            return;
        } catch (BrokenBarrierException | TimeoutException e) {
            try {
                presentToAssistant();
            } catch (InterruptedException ex) {
                timeoutPresentation(10);
                return;
            }
        }

        finishPresentation(10);
    }

    private void waitToPresent() throws InterruptedException {
        if (!canPresent()) {
            synchronized (classroom.getLock()) {
                classroom.getLock().wait();
            }
        }
    }

    private boolean canPresent() {
        return classroom.getProfessor().isProfessorReady() || classroom.getAssistant().isAssistantReady();
    }

    private void presentToProfessor() throws InterruptedException, TimeoutException, BrokenBarrierException {
        final Professor professor = classroom.getProfessor();

        professor.getInBarrier().await(PROFESSOR_TIMEOUT, TimeUnit.MILLISECONDS);
        professor.setProfessorReady(false);
        Thread.sleep(presentationTime);
        professor.getOutBarrier().await();
        professor.setProfessorReady(true);
        classroom.callForProfessor();
    }

    private void presentToAssistant() throws InterruptedException {
        final Assistant assistant = classroom.getAssistant();

        assistant.setAssistantReady(false);
        Thread.sleep(presentationTime);
        assistant.setAssistantReady(true);
        classroom.callForAssistant();
    }

    // fixed score for easier testing
    private void finishPresentation(int score) {
        classroom.addScore(score);
        System.out.println(getInfoForPrint(score));
        unlockClassroom();
    }

    // fixed score for easier testing
    private void timeoutPresentation(int score) {
        classroom.addScore(score);
        System.err.println(getInfoForPrint(score));
        unlockClassroom();
    }

    private String getInfoForPrint(int score) {
        return String.format("Thread:%s Arrival:%d TCC:%d Score:%d",
                Thread.currentThread().getName(), delayTime, presentationTime, score);
    }

    private static int counter;

    private void unlockClassroom() {
        if (counter == classroom.getStudents().size() - 1) {
            synchronized (classroom) {
                classroom.notify();
            }
        }

        counter++;
    }

    @Override
    public String toString() {
        return String.format("Student:[presentationTime=%d, delayTime=%d]", presentationTime, delayTime);
    }
}
