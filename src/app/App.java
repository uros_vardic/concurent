package app;

import threads.Classroom;

import java.util.concurrent.*;

public class App {

    private static final int NUMBER_OF_STUDENTS = 19;

    private static final long TIMEOUT = 5000L;

    private static final long TERMINATION_TIMEOUT = 500L;

    public static void main(String[] args) {
        final ExecutorService classroomExecutor = Executors.newFixedThreadPool(1);

        final Classroom classroom = new Classroom(NUMBER_OF_STUDENTS);

        final Future<String> classroomFuture = classroomExecutor.submit(classroom);

        classroomExecutor.shutdown();

        try {
            System.out.println(classroomFuture.get(TIMEOUT, TimeUnit.MILLISECONDS));
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.err.println("System timed out!");
            System.err.println("Students that didn't finish:");
            classroom.shutdownClassroom();
            classroom.printLateStudents();
        }

        try {
            if (!classroomExecutor.awaitTermination(TERMINATION_TIMEOUT, TimeUnit.MICROSECONDS))
                classroomExecutor.shutdownNow();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
