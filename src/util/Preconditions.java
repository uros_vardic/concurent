package util;

public class Preconditions {

    private Preconditions() {}

    public static <T> T checkNotNull(T reference) {
        if (reference == null)
            throw new NullPointerException();

        return reference;
    }

    public static <T> T checkNotNull(T reference, String errorMessage) {
        if (reference == null)
            throw new NullPointerException(errorMessage);

        return reference;
    }

    public static void checkArgument(boolean expression) {
        if (!expression)
            throw new IllegalArgumentException();
    }

    public static void checkArgument(boolean expression, String errorMessage) {
        if (!expression)
            throw new IllegalArgumentException(errorMessage);
    }

}
